class Post < ApplicationRecord
  belongs_to :user
  has_many :photos, dependent: :destroy
  
  accepts_nested_attributes_for :photos
   
  validates :user_id, presence: true
  validates :caption, presence: true, length: { maximum: 140 }
end
