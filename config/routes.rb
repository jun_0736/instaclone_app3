Rails.application.routes.draw do
   devise_for :users,
   controllers: { registrations: 'registrations' }
   
   root 'posts#index'
   
   resources :users do
    member do
      get :following, :followers
    end
   end
   
   #get '/users/:id', to: 'users#show', as: 'user'

   resources :posts, only: %i(new create index show destroy) do
    resources :photos, only: %i(create)
   end
   resources :relationships,       only: [:create, :destroy]
   
end
